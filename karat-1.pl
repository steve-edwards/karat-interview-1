#!/usr/bin/perl

use	5.010;
use	strict;
use	warnings;

use	Data::Dumper qw(Dumper);

my @logs1 = (
    ["58523", "user_1", "resource_1"],
    ["62314", "user_2", "resource_2"],
    ["54001", "user_1", "resource_3"],
    ["200", "user_6", "resource_5"],
    ["215", "user_6", "resource_4"],
    ["54060", "user_2", "resource_3"],
    ["53760", "user_3", "resource_3"],
    ["58522", "user_22", "resource_1"],
    ["53651", "user_5", "resource_3"],
    ["2", "user_6", "resource_1"],
    ["100", "user_6", "resource_6"],
    ["400", "user_7", "resource_2"],
    ["100", "user_8", "resource_6"],
    ["54359", "user_1", "resource_3"],
);

my @logs2 = (
    ["300", "user_1", "resource_3"],
    ["599", "user_1", "resource_3"],
    ["900", "user_1", "resource_3"],
    ["1199", "user_1", "resource_3"],
    ["1200", "user_1", "resource_3"],
    ["1201", "user_1", "resource_3"],
    ["1202", "user_1", "resource_3"],
);

my @logs3 = (
    ["300", "user_10", "resource_5"],
);

# For each user in the passed array, return a hash containing the user
# name, first access, and last access.
sub					user_access()
	{
	my ($log) = @_;
	my %accesses;
	foreach	my $access (@$log)
		{
#$DB::single = 1;
#		say Dumper $access;
#		say "@$access";
		my $time = $access->[0];
		my $name = $access->[1];
# new user
		if	(not exists $accesses{$name})
			{
			$accesses{$name}{'first_access'}
				= $accesses{$name}{'last_access'}
				= $time;
			next;
			}
# first access
		if	($time < $accesses{$name}{'first_access'})
			{
			$accesses{$name}{'first_access'} = $time;
			}
# last access
		if	($time > $accesses{$name}{'last_access'})
			{
			$accesses{$name}{'last_access'} = $time;
			}
		}

#$DB::single = 1;
#	say Dumper %accesses;
	return(\%accesses);
	}

say 'log1 ' . Dumper &user_access(\@logs1);
say 'log2 ' . Dumper &user_access(\@logs2);
say 'log3 ' . Dumper &user_access(\@logs3);
